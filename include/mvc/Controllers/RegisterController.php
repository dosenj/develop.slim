<?php

namespace App\mvc\Controllers;

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use App\mvc\Models\User;

class RegisterController extends Controller

{
	public function registerForm($request, $response)
	{
		$this->view->render($response, 'register.twig');
	}

	public function registerData($request, $response)
	{
		$name = $request->getParam('name');
		$email = $request->getParam('email');
		$password = $request->getParam('password');

		$validateName = v::alnum()->noWhitespace()->length(3, 30)->notEmpty();
		$validateEmail = v::notEmpty()->email();
		$validatePassword = v::notEmpty()->length(5, 30);

		try {
		    $validateName->assert($name);
		} catch(NestedValidationException $exception) {
			$nameErrors = $exception->getMessages();
		}

		try {
			$validateEmail->assert($email);
		} catch(NestedValidationException $exception) {
			$emailErrors = $exception->getMessages(); 
		}

		try {
			$validatePassword->assert($password);
		} catch(NestedValidationException $exception){
			$passwordErrors = $exception->getMessages();
		}

		if($nameErrors || $emailErrors || $passwordErrors){
			$this->view->render($response, 'register.twig', [
				'nameErrors' => $nameErrors, 'emailErrors' => $emailErrors, 'passwordErrors' => $passwordErrors
			]);
		}

		$user = User::create(['name' => $name, 'email' => $email, 'password' => password_hash($password, PASSWORD_DEFAULT)]);

		if($user){
			$this->flash->addMessage('Test', 'Register message');
			$this->logger->addInfo('New user registred');
			return $response->withStatus(302)->withHeader('Location', 'homepage');
		}

	}

	public function homepage($request, $response)
	{
		$flashMessage = $this->flash->getMessages();

		$this->view->render($response, 'homepage.twig', ['flashMessage' => $flashMessage]);
	}
}