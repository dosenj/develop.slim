<?php

namespace App\mvc\Controllers;

use App\mvc\Models\User;

class TestController extends Controller

{
	public function test()
	{
		echo "Test function, test controller";
	}

	public function development($request, $response)
	{
		$users = User::all();
		$this->view->render($response, 'test.twig', ['users' => $users]);
	}
}