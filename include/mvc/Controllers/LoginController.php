<?php

namespace App\mvc\Controllers;

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use App\mvc\Models\User;

class LoginController extends Controller

{
	public function loginForm($request, $response)
	{
		$this->view->render($response, 'login.twig');
	}

	public function loginData($request, $response)
	{
		$email = $request->getParam('email');
		$password = $request->getParam('password');
		$checked = $request->getParam('save');

		$validateEmail = v::notEmpty()->email();
		$validatePassword = v::notEmpty()->length(5, 30);

		try {
			$validateEmail->assert($email);
		} catch(NestedValidationException $exception) {
			$emailErrors = $exception->getMessages(); 
		}

		try {
			$validatePassword->assert($password);
		} catch(NestedValidationException $exception){
			$passwordErrors = $exception->getMessages();
		}

		if($emailErrors || $passwordErrors){
			$this->view->render($response, 'login.twig', [
				'emailErrors' => $emailErrors, 'passwordErrors' => $passwordErrors
			]);
		}

		$user = User::where('email', $email)->first();

		if($user){
			$checkPassword = password_verify($password, $user->password);
			if($checkPassword){
				if($checked == 'on'){
					setcookie('user', $email, time() + (86400 * 30), "/");
				}
				return $response->withStatus(302)->withHeader('Location', 'homepage');
			} else {
				$message = 'Wrong password';
				$this->view->render($response, 'login.twig', ['message' => $message]);
			}
		}
	}
}