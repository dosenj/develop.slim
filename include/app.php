<?php

session_start();

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/settings/settings.php';

$app = new \Slim\App($settings);

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($settings['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/mvc/Views', [
        'cache' => false
    ]);
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));
    return $view;
};

$container['flash'] = function ($container) {
    return new \Slim\Flash\Messages();
};

$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('Page not found try again!');
    };
};

$container['logger'] = function ($container) {
    $logger = new \Monolog\Logger('Logger');
    $file_handler = new \Monolog\Handler\StreamHandler(__DIR__ . '/logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['TestController'] = function($container) {
	return new \App\mvc\Controllers\TestController($container);
};

$container['RegisterController'] = function($container) {
	return new \App\mvc\Controllers\RegisterController($container);
};

$container['LoginController'] = function($container) {
	return new \App\mvc\Controllers\LoginController($container);
};

$container['DevController'] = function($container) {
    return new \App\mvc\Controllers\DevController($container);
};

require __DIR__ . '/../include/routes/routes.php';