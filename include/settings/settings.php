<?php

return $settings = [
	'settings' => [
	    'determineRouteBeforeAppMiddleware' => false,
	    'displayErrorDetails' => true,
	    'db' => [
	        'driver' => 'mysql',
	        'host' => 'localhost',
	        'database' => 'slim',
	        'username' => 'root',
	        'password' => 'aurora',
	        'charset'   => 'utf8',
	        'collation' => 'utf8_unicode_ci',
	        'prefix'    => '',
		]
	]
];