<?php

// TEST ROUTES

$app->get('/test', function() {
	echo "This is test function";
});

$app->get('/dev', 'TestController:test')->setName('dev');

$app->get('/development', 'TestController:development')->setName('development');

// REGISTER ROUTES

$app->get('/register', 'RegisterController:registerForm')->setName('register');
$app->post('/register', 'RegisterController:registerData');

// LOGIN ROUTES

$app->get('/login', 'LoginController:loginForm')->setName('login');
$app->post('/login', 'LoginController:loginData');

// HOMEPAGE ROUTES

$app->get('/homepage', 'RegisterController:homepage')->setName('homepage');

// DEV ROUTES

$app->get('/slim/dev', 'DevController:showSomeFakeData')->setName('slim.dev');